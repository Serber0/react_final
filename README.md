### Trabajo final react
<em> ##Bienvenido a mi repositorio para la creacion de una HV (SPA)</em>


### Caracteristicas
<em>
Se utilizo vite para la creacion del proyecto de react
    -https://vitejs.dev/
Se manejaron los commits por medio de gitbash 
    -https://git-scm.com/book/es/v2/Ap%C3%A9ndice-A%3A-Git-en-otros-entornos-Git-con-Bash
Se subió el proyecto a un repositorio en Gitlab
    -https://gitlab.com/
Se manejó un formulario de contacto 
    -https://freefrontend.com/css-forms/
Se manejaron tabs para mostrar información
    -https://alvarotrigo.com/blog/html-css-tabs/</em>


### Autor 
<em>Sergio Alejandro Bermudez Ortegon</em>
<em>https://gitlab.com/Serber0/react_final</em>