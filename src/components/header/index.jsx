import React from "react";
import { Alert, Button } from 'react-bootstrap';
const Aux = () => {
    return (
        <div class='container'>
            <div class='row'>
                <div class="tabs">
                    <div class="tab">
                        <input type="radio" name="css-tabs" id="tab-1" checked class="tab-switch" />
                        <label for="tab-1" class="tab-label">Información Personal</label>
                        <div class="tab-content">
                            <p className="hv__info--personal">
                                <div class='letter__info_personal'>
                                    Lugar de Nacimiento: Manizales - Colombia<br />
                                    Fecha de nacimiento: 20 de Enero del 2023<br />
                                    Estado Civil: Soltero<br />
                                    Cedula de Ciudadania: 1002652860<br />
                                    Numero Celular: 3115284307<br />
                                    Correo Electronico:soenmc00@gmail.com
                                </div>
                            </p>
                        </div>
                    </div>
                    <div class="tab">
                        <input type="radio" name="css-tabs" id="tab-2" class="tab-switch" />
                        <label for="tab-2" class="tab-label">Informacion Académica</label>
                        <div class="tab-content">
                            <p className="hv__info--academic">
                                <div class='letter__info_academic'>
                                    Basica Primaria: Inem Baldomero Sanín Cano <br />
                                    Bachillerato Tecnico: Inem Baldomero Sanín Cano <br />
                                    Titulo Universitario: Universidad de Manizales
                                </div>
                            </p>
                        </div>
                    </div>
                    <div class="tab">
                        <input type="radio" name="css-tabs" id="tab-3" class="tab-switch" />
                        <label for="tab-3" class="tab-label">Experiencia Laboral</label>
                        <div class="tab-content">
                            <p className="hv__info--exp">
                                <div class='letter__info_exp'>
                                    Empresa: Edificio de la judicatura <br />
                                    Empresa: Hotel Opalo <br />
                                    Empresa: Lavautos los tanques
                                    Empresa: Emergia
                                </div>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form">
                <div class="title">CONTACTO</div>
                <div class="subtitle">PROGRAMADOR</div>
                <div class="input-container ic2">
                    <placeholder class='primer_cuadro'>Nombre</placeholder>
                    <input id="firstname" class="input" type="text" placeholder=" " />

                </div>
                <div class="input-container ic2">
                    <placeholder class='primer_cuadro'>Asunto</placeholder>
                    <input id="Email" class="input" type="text" placeholder=" " />
                </div>
                <div class="input-container ic2">
                    <placeholder class='primer_cuadro'>Email</placeholder>
                    <input id="Asunto" class="input" type="text" placeholder=" " />
                </div>
                <div class="input-container ic2">
                    <placeholder class='primer_cuadro'>Mensaje</placeholder>
                    <input id="email" class="input" type="text" placeholder="" />
                </div>
                <button type="text" class="submit">submit</button>
            </div>
            <hr class="slash--3"></hr>
        </div>
        
    );
}

export default Aux;