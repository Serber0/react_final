import React from "react";
import photo from "./images/Captura.jpg";

const Footer = () => {
    return (
        <div className="HV">
            <div className="hv__main">
                <h1 className="hv__main--content"></h1>
            </div>
            <div className="hv__info">
                <hr class="slash--1"></hr>
                <p className="hv__info--title">HOJA DE VIDA</p> <br />
                <p className="hv__header--name">Sergio A. Bermudez Ortegon</p>
                <img class="hv__photo" src={photo} />
                <p className="hv__info--profile">Perfil</p>
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <p class='profile__desc'>
                                Ingeniero de sistemas y telecomunicaciones, experiencia en atencion al cliente y
                                resolucion de problemas. Soy una persona responsable, honesta y bastante dinamica
                                con deseos de superacion, mis metas estan basadas en el logro de objetivos claros
                                aprendo con facilidad y cumplo de manera adecuada con puntualidad, honestidad y responsabilidad
                                las distintas tareas y actividades asignadas.
                            </p>
                            <hr class="slash--2"></hr>
                            <hr class="slash--3"></hr>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
export default Footer;
