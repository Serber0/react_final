import { useState } from 'react'
import './App.css'
import 'bootstrap/dist/css/bootstrap.min.css';

import Aux from "./components/header"
import Footer from './components/footer';


function App() {

  return (
    <> 
      
      <Footer></Footer>
      <Aux></Aux>
    </>
  )
}

export default App
